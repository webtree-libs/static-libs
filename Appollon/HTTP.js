function initXMLHttpRequestServerPing(jwtoken, sid){

    _url = "{{ site.link }}/wp-json/rabbit-hole/v1/server/comm/ping";
    _data = {'sid': sid};
    jQuery('#sid' + sid + '')
        .html('Status: <span class="label label-info">pinging..</span>');
    let xmlHttpRequest = new XMLHttpRequest();
    let params = _data;
    xmlHttpRequest.open("POST", _url);
    xmlHttpRequest.setRequestHeader('Authorization', 'Bearer ' + jwtoken);
    xmlHttpRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttpRequest.send(JSON.stringify(params));

    xmlHttpRequest.onreadystatechange = function () {
        if (xmlHttpRequest.readyState == 4) {
            if (xmlHttpRequest.status == 200) {
                const Data = JSON.parse(xmlHttpRequest.response);
                jQuery('#sid'+sid+'')
                    .html('Status: <span class="label label-success">online</span>');
                // console.error(Data);
            }
        } else {
            jQuery('#sid'+sid+'')
                .html('Status: <span class="label label-danger">offline</span>');
            return 'else';

        }
    };

}

function makeAJAXCall(methodType, url, callback, data){
    var xhr = new XMLHttpRequest();
    xhr.open(methodType, url, true);

    // console.log(data);

    xhr.send(JSON.stringify(data));

    xhr.onreadystatechange = function(){
        // console.log(xhr.readyState);
        // console.log(xhr.status);
        if (xhr.readyState === 4 && xhr.status === 200){
            callback(xhr.response);
        }
    };
    // console.log("request sent to the server");
}